﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class driving : MonoBehaviour {

    private Rigidbody rbody;
    private RigidbodyConstraints originFrezze;
    [SerializeField]
    private float force,forceup=0;
    [SerializeField]
    private float posYstart, posZstart;
    private bool pressSpace = false;
    [SerializeField]
    private AudioSource goingdown, goingup;


    void Start () {
        rbody = GetComponent<Rigidbody>();
        posYstart = this.transform.localPosition.y;
        posZstart = this.transform.localPosition.z;
        originFrezze = rbody.constraints;
    }

    // Update is called once per frame
    void Update () {
        
        if(Input.GetKeyDown("space"))
        {
            goingdown.Play();
        }
        else if (Input.GetKey("space"))
        {
            pressSpace = true;
            rbody.AddForce(-transform.forward * force);
            rbody.constraints = originFrezze;
        }
        else if(Input.GetKeyUp("space"))
        {
            goingdown.Stop();
            goingup.Play();
            pressSpace = false; 
        }
        else if (pressSpace==false)
        {
            // rbody.AddForce(transform.forward * forceup);
            addForceUP();
        }
    }

    void addForceUP()
    {
        if (this.transform.localPosition == new Vector3(this.transform.localPosition.x, posYstart, posZstart))
        {
            rbody.constraints = RigidbodyConstraints.FreezeAll;
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, posYstart, posZstart);
            pressSpace = true;
        }
        else
        {
            rbody.AddForce(transform.forward * forceup);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="StopDriving")
        {
            rbody.constraints = RigidbodyConstraints.FreezeAll;
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, posYstart, posZstart);
        }
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "StopDriving")
        {
            rbody.constraints = RigidbodyConstraints.FreezeAll;
            this.transform.localPosition =new Vector3(this.transform.localPosition.x, posYstart, posZstart);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "StopDriving")
        {
            rbody.constraints = originFrezze;
        }
    }
}
