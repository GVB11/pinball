﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour {

    private Rigidbody rbody;
    public float gravityforce;
    [SerializeField]
    private GameObject Pinball;
    [SerializeField]
    private bool bugg=false;

	// Use this for initialization
	void Start () {
        rbody = GetComponent<Rigidbody>();
        Pinball = GameObject.Find("Pinball");
        this.transform.parent = Pinball.transform;
    }
	
	// Update is called once per frame
	void Update () {
        if (bugg == false)
        {
            rbody.AddForce(Physics.gravity * gravityforce);
        }
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "destroyballs")
        {
            Destroy(this.gameObject);
            GameControler.instance.UpdateBall(0, 1,true);
            GameControler.instance.GoReturnBox();
        }
        if(other.gameObject.tag=="boxmove")
        {
            GameControler.instance.GoBoxMove();
        }
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "driving")
        {
            bugg = true;
        }
    }
    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "driving")
        {
            bugg = false;
        }
    }
}
