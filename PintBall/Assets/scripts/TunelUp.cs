﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TunelUp : MonoBehaviour {

    [SerializeField]
    private AudioSource audioo;
    private Rigidbody rbody;
    [SerializeField]
    private bool activateHitter,unfrezze;
    [SerializeField]
    private float timer= 0;
    [SerializeField]
    private GameObject ball,lighter;

    void Start()
    {
        rbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if(activateHitter)
        {
            GameControler.instance.UpdateScore(8111);
            activateHitter = false;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ball")
        {
            other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            StartCoroutine(GameControler.instance.BallinTunel());
            lighter.SetActive(true);
            audioo.PlayOneShot(this.audioo.clip, 1);
            activateHitter = true;
        }
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "ball")
        {
            timerrr();
            if (unfrezze == true)
            {
                other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                lighter.SetActive(false);
                unfrezze = false;
            }
        }
    }
    void timerrr()
    {
        timer = timer + Time.deltaTime;
       if (timer >= 2)
        {
            unfrezze = true;
            timer = 0;
        }
    }
}
