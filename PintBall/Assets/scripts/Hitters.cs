﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitters : MonoBehaviour {

    public enum Fases{verde,amarillo,rojo };
    public Fases faseshits=Fases.verde;
    [SerializeField]
    GameObject lighterV,lightA,lightR;
    private bool activateHitter=false;
    public bool isred,stopred = false;
    [SerializeField]
    float timer,timesection1,forces,forceradius=0;
    [SerializeField]
    private AudioSource audioo;

	void Update () {
        if (activateHitter)
        {
           if(faseshits==Fases.verde)
            {
                lightR.SetActive(false);
                lighterV.SetActive(true);
                Timer();
            }
           else if(faseshits==Fases.amarillo)
            {
                lightA.SetActive(true);
                Timer();
            }
           else if(faseshits==Fases.rojo)
            {
                lightR.SetActive(true);
                Timer();
            }
        }
    }
 
    void Timer()
    {
        timer = timer + Time.deltaTime;
        if (timer >= 0.5f)
        {
            if (faseshits == Fases.verde)
            {
                GameControler.instance.UpdateScore(500);
                lighterV.SetActive(false);
                faseshits = Fases.amarillo;
            }
            else if (faseshits == Fases.amarillo)
            {
                GameControler.instance.UpdateScore(750);
                lightA.SetActive(false);
                faseshits = Fases.rojo;
            }
            else if (faseshits ==Fases.rojo)
            {
                GameControler.instance.UpdateScore(1000);
                lightR.SetActive(false);
                ActivateRed();
                //isred = true;
                //faseshits = Fases.verde;
            }
            this.transform.localScale = this.transform.localScale / 1.1f;
            timer = 0;
            activateHitter = false;
        }
    }
    void OnCollisionEnter(Collision other)
    {
    /*    foreach(Collider col in Physics.OverlapSphere(transform.position, forceradius))
        {
            if(col.GetComponent<Rigidbody>())
            {
                col.GetComponent<Rigidbody>().AddExplosionForce(forces, transform.position, forceradius);
            }
        }*/

        if (other.gameObject.tag == "ball")
        {
            audioo.PlayOneShot(this.audioo.clip,1);
            activateHitter = true;
            this.transform.localScale = this.transform.localScale * 1.1f;
        }
    }
    void ActivateRed()
    {
        if(stopred==false)
        {
            isred = true;
            stopred = true;
        }
    }


}
