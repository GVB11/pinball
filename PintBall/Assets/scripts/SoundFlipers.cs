﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFlipers : MonoBehaviour {

    [SerializeField]
    private AudioSource fliper;

    void Update () {

        if (Input.GetKeyDown("left") || Input.GetMouseButtonDown(0))
        {
            fliper.Play();
        }
        else if (Input.GetKeyDown("right") || Input.GetMouseButtonDown(1))
        {
            fliper.Play();
        }
    }
}
