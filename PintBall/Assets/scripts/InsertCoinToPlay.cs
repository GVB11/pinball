﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsertCoinToPlay : MonoBehaviour {

    public bool insertCoin = false;
    [SerializeField]
    private AudioSource audiocoin;


    void OnMouseDown()
    {
        if (insertCoin == false)
        {
            audiocoin.Play();
            GameControler.instance.GameBalls = 3;
            GameControler.instance.Score = 0;
            GameControler.instance.UpdateScore(0);
            GameControler.instance.UpdateBall(0, 0, false);
            GameControler.instance.state = GameControler.GameState.running;
            GameControler.instance.GameStateVoid();
            ManagerBumpers.instance.ResetSection1();
            ManagerBumpers.instance.ActiveBumperSection1();
            ManagerBumpers.instance.ResetSection2();
            ManagerBumpers.instance.ActiveBumperSection2();

            insertCoin = true;
        }
    }
}
