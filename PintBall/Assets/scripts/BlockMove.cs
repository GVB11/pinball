﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMove : MonoBehaviour
{
    [SerializeField]
    AudioSource audioo,audioextraball;
    private Rigidbody rbody;
    [SerializeField]
    private float posZupdate;
    [SerializeField]
    private int forces;
    [SerializeField]
    private bool up, down, activateHitter;
    public enum Fases { verde, amarillo, rojo };
    public Fases faseshits = Fases.verde;
    [SerializeField]
    GameObject lighterV, lightA, lightR;
    private float timer = 0;

    void Start()
    {
        rbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePosition();
        AddForces();
        ActivateLight();
    }

    void UpdatePosition()
    {
        posZupdate = this.transform.localPosition.z;
        if (posZupdate <= -0.8)
        {
            up = true;
            down = false;
        }
        else if (posZupdate >= 1.5f)
        {
            down = true;
            up = false;
        }
    }

    void AddForces()
    {
        if (up == true)
        {
            rbody.AddForce(transform.forward * forces);
        }
        else if (down == true)
        {
            rbody.AddForce(-transform.forward * forces);
        }
    }

    void ActivateLight()
    {
        if (activateHitter)
        {
            if (faseshits == Fases.verde)
            {
                lighterV.SetActive(true);
                Timer();
            }
            else if (faseshits == Fases.amarillo)
            {
                lightA.SetActive(true);
                Timer();
            }
            else if (faseshits == Fases.rojo)
            {
                lightR.SetActive(true);
                Timer();
            }
        }
    }

    void Timer()
    {
        timer = timer + Time.deltaTime;
        if (timer >= 0.5f)
        {
            if (faseshits == Fases.verde)
            {
                GameControler.instance.UpdateScore(122);
                lighterV.SetActive(false);
                faseshits = Fases.amarillo;
            }
            else if (faseshits == Fases.amarillo)
            {
                GameControler.instance.UpdateScore(183);
                lightA.SetActive(false);
                faseshits = Fases.rojo;
            }
            else if (faseshits == Fases.rojo)
            {
                GameControler.instance.UpdateScore(211);
                GameControler.instance.UpdateBall(1, 0, false);
                GameControler.instance.UpdateText("NICE YOU WIN \n EXTRA BALL",true);
                audioextraball.Play();
                lightR.SetActive(false);
                faseshits = Fases.verde;
            }
            timer = 0;
            activateHitter = false;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ball")
        {
            audioo.PlayOneShot(this.audioo.clip, 1);
            activateHitter = true;
        }
    }
}