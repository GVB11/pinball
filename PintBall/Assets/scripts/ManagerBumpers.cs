﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerBumpers : MonoBehaviour {
    public static ManagerBumpers instance;
    [SerializeField]
    private GameObject bumper1, bumper2, bumper3,bumper4,bumper5;
    private bool section1, section2, messagesec1, messagesec2, stopbool ,stopbool2= false;
    [SerializeField]
    private float timer=0;
    [SerializeField]
    private AudioSource audioscorex3,audiootherball;

    void Awake()
    {
        instance = this;
    }


    void Update () {
        ManagerSection1();
        ManagerSection2();
        if(messagesec1==true && stopbool==false)
        {
            GameControler.instance.StartCoroutine(GameControler.instance.Bumpersection1Out());
            stopbool = true;
        }
        if(messagesec2==true && stopbool2==false)
        {
            GameControler.instance.StartCoroutine(GameControler.instance.Bumpersection2Out());
            stopbool2 = true;
        }
    }
   
    private void ManagerSection1()
    {
        if (bumper1.GetComponent<Hitters>().isred && bumper2.GetComponent<Hitters>().isred && bumper3.GetComponent<Hitters>().isred && GameControler.instance.state==GameControler.GameState.running)
        {
            timer = timer + Time.deltaTime;
            GameControler.instance.UpdateText("SCORE \n X3",false);
            ActiveBumperSection1();
            if (timer <= 0.575f) audioscorex3.Play();
            else if (timer <= 10)
            {
                GameControler.instance.scorex3 = true;
            }
            else if (timer >= 10)
            {
                messagesec1 = false;
                stopbool = false;
                GameControler.instance.UpdateText(" ",true);
                GameControler.instance.scorex3 = false;
                ResetSection1();
                timer = 0;
            }
        }
        else
        {
            if (bumper1.GetComponent<Hitters>().isred)
            {
                bumper1.transform.parent.gameObject.SetActive(false);
                messagesec1 = true;
            }
             if (bumper2.GetComponent<Hitters>().isred)
            {
                bumper2.transform.parent.gameObject.SetActive(false);
                messagesec1 = true;
            }
             if (bumper3.GetComponent<Hitters>().isred)
            {
                bumper3.transform.parent.gameObject.SetActive(false);
                messagesec1 = true;
            }
        }
    }
    public void ResetSection1()
    {
        bumper1.GetComponent<Hitters>().isred = false;
        bumper2.GetComponent<Hitters>().isred = false;
        bumper3.GetComponent<Hitters>().isred = false;
        bumper1.GetComponent<Hitters>().stopred = false;
        bumper2.GetComponent<Hitters>().stopred = false;
        bumper3.GetComponent<Hitters>().stopred = false;
        bumper1.GetComponent<Hitters>().faseshits = Hitters.Fases.verde;
        bumper2.GetComponent<Hitters>().faseshits = Hitters.Fases.verde;
        bumper3.GetComponent<Hitters>().faseshits = Hitters.Fases.verde;
    }
    public void ActiveBumperSection1()
    {
        bumper1.transform.parent.gameObject.SetActive(true);
        bumper2.transform.parent.gameObject.SetActive(true);
        bumper3.transform.parent.gameObject.SetActive(true);
    }

    private void ManagerSection2()
    {
        if (bumper4.GetComponent<Hitters>().isred && bumper5.GetComponent<Hitters>().isred && GameControler.instance.state == GameControler.GameState.running)
        {
            audiootherball.Play();
            ActiveBumperSection2();
            ResetSection2();
            GameControler.instance.UpdateBall(1, 0, false);
            GameControler.instance.GenerateOtherBall();
            GameControler.instance.GoReturnBox();
            messagesec2 = false;
            stopbool2 = false;
        }
        else
        {
            if (bumper4.GetComponent<Hitters>().isred)
            {
                bumper4.transform.parent.gameObject.SetActive(false);
                messagesec2 = true;
            }
            if (bumper5.GetComponent<Hitters>().isred)
            {
                bumper5.transform.parent.gameObject.SetActive(false);
                messagesec2 = true;
            }
        }
    }
    public void ResetSection2()
    {
        bumper4.GetComponent<Hitters>().isred = false;
        bumper5.GetComponent<Hitters>().isred = false;
        bumper4.GetComponent<Hitters>().stopred = false;
        bumper5.GetComponent<Hitters>().stopred = false;
        bumper4.GetComponent<Hitters>().faseshits = Hitters.Fases.verde;
        bumper5.GetComponent<Hitters>().faseshits = Hitters.Fases.verde;
    }
    public void ActiveBumperSection2()
    {
        bumper4.transform.parent.gameObject.SetActive(true);
        bumper5.transform.parent.gameObject.SetActive(true);
    }
}
