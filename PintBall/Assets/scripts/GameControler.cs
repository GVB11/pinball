﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControler : MonoBehaviour {

    // Use this for initialization
    public static GameControler instance;
    public enum GameState { start, running, gameover };
    public GameState state = GameState.start;
    public GameObject Ball,Generator,boxMove,PanelPinBallScore, PanelPinBallBalls,insertcoin;
    public int GameBalls=3;
    public int Score;
    public bool scorex3=false;
    Text textScore, textBall,textn;
    public AudioSource bg,ballregenerate,counterscore;

    void Awake () {
        instance = this;
        insertcoin = GameObject.Find("insertcointoplay/colider");
        Generator = GameObject.Find("GeneratorBalls");
        boxMove = GameObject.Find("boxMove");
        PanelPinBallScore=GameObject.Find("Canvas/PanelPinBall/TextScore");
        PanelPinBallBalls = GameObject.Find("Canvas/PanelPinBall/TextBalls");
        textScore = GameObject.Find("Canvas/PanelPinBall/TextScore/Score").GetComponent<Text>();
        textBall = GameObject.Find("Canvas/PanelPinBall/TextBalls/Balls").GetComponent<Text>();
        textn = GameObject.Find("Canvas/Text").GetComponent<Text>();
        bg.Play();
        GameStateVoid();
    }

    public void GameStateVoid()
    {
        if (state == GameState.start)
        {
            PanelPinBallBalls.SetActive(false);
            PanelPinBallScore.SetActive(false);
            textn.text =("INSERT COIN \n TO PLAY");
        }
        else if(state==GameState.running)
        {
            PanelPinBallBalls.SetActive(true);
            PanelPinBallScore.SetActive(true);
            StartCoroutine(WaitForBall());
        }
        else if(state==GameState.gameover)
        {
            counterscore.Play();
            textn.text = "YOU LOSE YOUR SCORE: " + Score + "\n INSERT COIN TO PLAY AGAIN";
            PanelPinBallBalls.SetActive(false);
            PanelPinBallScore.SetActive(false);
            insertcoin.GetComponent<InsertCoinToPlay>().insertCoin = false;
        }
    }
public void GenerateOtherBall()
    {
        if (GameBalls >= 0)
        {
            ballregenerate.Play();
            Instantiate(Ball, new Vector3(Generator.transform.localPosition.x, Generator.transform.localPosition.y, Generator.transform.localPosition.z), transform.rotation);
        }
    }
    public void UpdateScore(int addToScore)
    {
        if (scorex3 == true)
        {
            Score += addToScore*3;
            textScore.text = " " + Score;
        }
        else
        {
            Score += addToScore;
            textScore.text = " " + Score;
        }
    }
    public void UpdateBall(int addBall, int removeBall, bool destroy)
    {
        GameBalls += addBall;
        GameBalls -= removeBall;
        textBall.text = " " + GameBalls;
        if(destroy==true)
        {
            StartCoroutine(WaitUpdateScoreDestroy());
        }
    }
    public void UpdateText(string texti, bool boleana)
    {
        textn.text = " " + texti;
        if (boleana)
        {
            StartCoroutine(coroutineUpdateText(boleana));
        }
    }

    public void GoBoxMove()
    {
        boxMove.GetComponent<boxMove>().closeDriving = true;
    }
    public void GoReturnBox()
    {
        boxMove.GetComponent<boxMove>().OpenDriving = true;
    }
    private IEnumerator coroutineUpdateText(bool leana)
    {
        yield return new WaitForSeconds(1.5f);
        textn.text = " ";
        leana = false;
    }
    private IEnumerator WaitForBall()
    {
        if (GameBalls >= 0)
        {
            textn.text = "READY... ";
            yield return new WaitForSeconds(1.5f);
            textn.text = "READY... \n GO!!!";
            yield return new WaitForSeconds(0.5f);
            GenerateOtherBall();
            textn.text = " ";
        }
        else
        {
            state = GameState.gameover;
            GameStateVoid();
        }
    }
    private IEnumerator WaitUpdateScoreDestroy()
    {
        textn.text = "OHH... ";
        yield return new WaitForSeconds(1);
        textn.text = "OHH... \n LOST BALL";
        yield return new WaitForSeconds(1);
        textn.text = " ";
       // yield return new WaitForSeconds(0.5f);
        GameStateVoid();
    }
    public IEnumerator ClosingGate()
    {
        textn.text = "CLOSING \n GATE";
        yield return new WaitForSeconds(1);
        textn.text = " ";
    }
    public IEnumerator BallinTunel()
    {
        textn.text = "WOOW! \n BALL GONE";
        yield return new WaitForSeconds(2);
        textn.text = " ";
    }
    public IEnumerator Bumpersection1Out()
    {
        textn.text = "BUMPER IS \n OUT";
        yield return new WaitForSeconds(1);
        textn.text = "DOWN 3 TO GET \n SCORE X3";
        yield return new WaitForSeconds(2);
        textn.text = " ";
    }
    public IEnumerator Bumpersection2Out()
    {
        textn.text = "BUMPER IS \n OUT";
        yield return new WaitForSeconds(1.5f);
        textn.text = "DOWN OTHER \n FOR 2 BALLS";
        yield return new WaitForSeconds(2);
        textn.text = " ";
    }
}
