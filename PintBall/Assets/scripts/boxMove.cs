﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxMove : MonoBehaviour {

    private Rigidbody rbody;
    private RigidbodyConstraints originFrezze;
    public bool closeDriving,OpenDriving;
    [SerializeField]
    private int forces;
    [SerializeField]
    private bool moveright,moveleft;
    private bool activateHitter = false;
    [SerializeField]
    float timer = 0;
    [SerializeField]
    private AudioSource audioo;
    [SerializeField]
    private GameObject lighter;

    void Start () {
        rbody = GetComponent<Rigidbody>();
        originFrezze = rbody.constraints;
    }
	
	void Update () {
       if (closeDriving==true)
        {
            rbody.constraints = originFrezze;
            moveright = true;
            Doforces();
            if (this.transform.localPosition.x>=3.95f || this.transform.localPosition.z>=0.673f)
            {
                rbody.constraints = RigidbodyConstraints.FreezeAll;
                StartCoroutine(GameControler.instance.ClosingGate());
                closeDriving = false;
                moveright = false; 
            }
        }
        else if(OpenDriving==true)
        {
            rbody.constraints = originFrezze;
            moveleft = true;
            Doforces();
            if(this.transform.localPosition.x<=3.56f || this.transform.localPosition.z <= 0.667)
            {
                rbody.constraints = RigidbodyConstraints.FreezeAll;
                OpenDriving = false;
                moveleft = false;
            }
        }

       if(activateHitter)
        {
            timer = timer + Time.deltaTime;
            lighter.SetActive(true);
            if(timer>=0.5f)
            {
                lighter.SetActive(false);
                GameControler.instance.UpdateScore(555);
                timer = 0;
                activateHitter = false;
            }
        }
	}
    
    void Doforces()
    {
        if(moveright==true)
        {
            Debug.Log("forces right");
            rbody.AddForce(-transform.right * forces);
        }
        else if(moveleft==true)
        {
            Debug.Log("forces left");
            rbody.AddForce(transform.right * forces);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ball")
        {
            audioo.PlayOneShot(this.audioo.clip, 1);
            activateHitter = true;
        }
    }
}
